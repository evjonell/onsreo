import { is_none, is_some, all_some, all_none, type Option } from './';

describe('Option', () => {
  class Foo {
    foo!: string;
  }

  class Bar {
    bar!: number;
  }

  let foo: [
    Option<number>,
    Option<typeof Foo>,
    Option<string>,
    Option<boolean>,
    Option<number>,
    Option<typeof Bar>,
    Option<string>,
  ] = [1, Foo, null, false, undefined, Bar, 'hello'];

  it('should test for some', () => {
    expect(is_some(foo[1])).toBeTruthy();
    expect(is_some(foo[2])).toBeFalsy();
  });

  it('should test for none', () => {
    expect(is_none(foo[1])).toBeFalsy();
    expect(is_none(foo[2])).toBeTruthy();
  });

  it('should test for all some', () => {
    expect(all_some([1, 2, true, false, 'hi', ''])).toBeTruthy();
    expect(all_some([1, null, true, undefined, 'hi', null])).toBeFalsy();
  });

  it('should test for all none', () => {
    expect(
      all_none([null, null, undefined, null, null, undefined]),
    ).toBeTruthy();
    expect(all_none([1, null, true, undefined, 'hi', null])).toBeFalsy();
  });
});
