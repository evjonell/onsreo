export declare type Some<T> = T;

export declare type None = null | undefined;

export declare type Option<T> = Some<T> | None;

export declare type Ok<T> = T extends Error ? never : T;

export declare type Err<E> = E extends Error ? E : never;

export declare type Result<T, E> = Ok<T> | Err<E>;

export declare type Tuple<T, U extends T[] = T[]> = {
  [P in keyof U]: U[P];
};

export declare type OptionTuple<T, U extends T[] = T[]> = {
  [P in keyof U]: U[P] extends Option<infer O> ? Option<O> : U[P];
};

export declare type SomeTuple<T, U extends T[] = T[]> = {
  [P in keyof U]: U[P] extends Option<infer O> ? Some<O> : Option<U[P]>;
};

export declare type NoneTuple<T, U extends T[] = T[]> = {
  [P in keyof U]: U[P] extends Option<any> ? None : Option<U[P]>;
};

export declare type ResultTuple<T, U extends T[] = T[]> = {
  [P in keyof U]: U[P] extends Result<infer O, infer E> ? Result<O, E> : U[P];
};

export declare type OkTuple<T, U extends T[]> = {
  [P in keyof U]: U[P] extends Result<infer O, infer _E> ? Ok<O> : U[P];
};

export declare type ErrTuple<T, U extends T[]> = {
  [P in keyof U]: U[P] extends Result<infer _T, infer E> ? Err<E> : U[P];
};

export function is_some<T>(val: Option<T>): val is Some<T> {
  if (
    !!val ||
    typeof val === 'number' ||
    typeof val === 'boolean' ||
    typeof val === 'string'
  )
    return true;
  return false;
}

export function is_none<T>(val: Option<T>): val is None {
  if (is_some(val)) return false;
  return true;
}

export function all_some<T, U extends T[] = T[]>(
  val: OptionTuple<T, U> | SomeTuple<T, U>,
): val is SomeTuple<T, U> {
  if (Array.isArray(val) && val.some(v => is_none(v))) return false;
  return true;
}

export function all_none<T, U extends T[] = T[]>(
  val: OptionTuple<T, U> | NoneTuple<T, U>,
): val is NoneTuple<T, U> {
  if (Array.isArray(val) && val.some(v => is_some(v))) return false;
  return true;
}

export function is_ok<T, E>(val: Result<T, E>): val is Ok<T> {
  if (val instanceof Error) return false;
  return true;
}

export function is_err<T, E>(val: Result<T, E>): val is Err<E> {
  if (val instanceof Error) return true;
  return false;
}

export function all_ok<T, U extends T[] = T[]>(
  val: ResultTuple<T, U> | OkTuple<T, U>,
): val is OkTuple<T, U> {
  if (Array.isArray(val) && val.some(v => is_err<any, any>(v))) return false;
  return true;
}

export function all_err<T, U extends T[] = T[]>(
  val: ResultTuple<T, U> | ErrTuple<T, U>,
): val is ErrTuple<T, U> {
  if (Array.isArray(val) && val.some(v => is_ok<any, any>(v))) return false;
  return true;
}
