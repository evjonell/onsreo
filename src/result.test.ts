import { is_ok, is_err, all_ok, all_err, Result } from './';

describe('Result', () => {
  class Foo {}

  let foo: [
    Result<number, Error>,
    Result<number, Error>,
    Result<Foo, Error>,
    Result<string, Error>,
    Result<string, Error>,
    Result<boolean, Error>,
    Result<boolean, Error>,
  ] = [1, new Error(), new Foo(), '', new Error(), true, new Error()];

  it('should test for Ok', () => {
    let bar = foo[2];
    if(is_ok(bar)){
      bar;
    }else {
      bar;
    }
    expect(is_ok(foo[2])).toBeTruthy();
    expect(is_ok(foo[4])).toBeFalsy();
  });

  it('should test for Err', () => {
    expect(is_err(foo[2])).toBeFalsy();
    expect(is_err(foo[4])).toBeTruthy();
  });

  it('should test for all Ok', () => {
    expect(
      all_ok([1, 2, new Foo(), Foo, true, false, 'hihi', '']),
    ).toBeTruthy();
    expect(
      all_ok([1, 2, new Foo(), new Error(), true, false, new Error(), '']),
    ).toBeFalsy();
  });

  it('should test for all Err', () => {
    expect(
      all_err([
        new Error(),
        new Error(),
        new Error(),
        new Error(),
        new Error(),
        new Error(),
        new Error(),
        new Error(),
      ]),
    ).toBeTruthy();
    expect(
      all_err([1, 2, new Foo(), Foo, true, false, 'hihi', '']),
    ).toBeFalsy();
  });
});
